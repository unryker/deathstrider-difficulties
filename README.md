### Notes
---
- Skill-levels are tuned for play with [Deathstrider.](https://gitlab.com/accensi/deathstrider)
- Information about each difficulty is explained on selection.
- Officially integrated with Deathstrider, but this version has some minor differences, like enemies no longer flinching on harder difficulties among other smaller things.